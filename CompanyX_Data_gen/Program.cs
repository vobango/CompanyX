﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace CompanyX_Data_gen
{
    class Program
    {
        static void Main(string[] args)
        {            
            using (CompanyXEntities compX = new CompanyXEntities())
            {
                //Generate employees
                string[] Names =
                {
                "Kõolemb Kuusk","Kõrb Mänd","Kõu Kask","Käbi Saar","Käblik Pöök","Kähr Tamm","Käpp Vaher",
                "Kärg Paakspuu","Kärme Sarapuu","Kylimit Pihlakas","Kyllelemb Künnap","Laas Jalakas","Lagle Toomingas",
                "Lahke Lehis","Laine Pärn"
                };

                Random r = new Random();

                compX.Employees.Add(new Employee
                {
                    FullName = "Lall Õunapuu",
                    Adddate = DateTime.Now,
                    Active = true,
                    IdCode = $"{r.Next(3, 5)}{r.Next(50, 91)}{DateTime.Now.ToString("MMddffff")}",
                    Rights = true
                });

                foreach (var name in Names)
                {
                    compX.Employees.Add(new Employee
                    {
                        FullName = name,
                        Adddate = DateTime.Now,
                        Active = true,
                        IdCode = $"{r.Next(3, 5)}{r.Next(50, 91)}{DateTime.Now.ToString("MMddffff")}",
                        Rights = false
                    });
                    Thread.Sleep(1);
                }
                compX.SaveChanges();

                //Generate skills for employees
                string[] Skills = { "Java", "Python", "C#", "C++", "JavaScript", "HTML", "CSS", "Excel", "SQL", "Windows", "Linux", "Mac", "Haskell", "R" };

                foreach (var person in compX.Employees)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        compX.Skills.Add(new Skill
                        {
                            IdCode = person.IdCode,
                            SkillName = Skills[r.Next(0, Skills.Length)],
                            Mastery = (r.Next(1, 4)).ToString()
                        });
                    }
                }
                compX.SaveChanges();
            }
        }
    }
}
